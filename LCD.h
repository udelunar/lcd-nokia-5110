#ifndef LCD_h
#define LCD_h

#include "arduino.h"

/**********************************************************************************************

	DESARROLLADO POR WWW.LUNEGATE.NET    // WWW.ROBOTIC-STUDIO NET

**********************************************************************************************/



class LCD
{                                                
    public:
		/**--- ENTRADAS DIGITALES PARA Contructor sin sobrecarga de entradas -----
		1,2,3,4,5,6,7 
		**/

        LCD();
        LCD( int PIN_RESET, int PIN_SCE, int PIN_DC, int PIN_SDIN, int PIN_SCLK, int PIN_LCD);
        LCD(byte contrast);
        LCD(byte contrast, int PIN_RESET, int PIN_SCE, int PIN_DC, int PIN_SDIN, int PIN_SCLK, int PIN_LCD);
        			
        //-- Metodos publicos --		
		/**
		 * x - Rango: 0 a 5
		 * y - Rango: 0 a 75
		 */
        void printText(int x, int y, char *characters);
        void scroll(int x, int y,String message); 
		/**
		 * Light =   Led display
		 * light = 0 (Very Shining)
		 * light = 10000 ( led out)
		 */
        void led(int light);  
		void drawBox();		
		
		
    private:
		/*-- GLOBAL --*/
		/* Configuration for the LCD*/
		#define LCD_C     LOW
		#define LCD_D     HIGH
		#define LCD_CMD   0

		// Size of the LCD
		#define LCD_X     84
		#define LCD_Y     48
		
		int scrollPosition;
		char strBuffer[30];  
		int txtCyclesMax;
		int txtCyclesCur;		
		static const byte ASCII[][5];
	   
		//-- VAR --
		int PIN_RESET;
		int PIN_SCE;
		int PIN_DC;
		int PIN_SDIN;
		int PIN_SCLK;
		int PIN_LCD;
		
        void configurar(byte contrast);
		void gotoXY(int x, int y);
		void LcdCharacter(char character);
		void LcdClear(void);
		void LcdInitialise(byte contrast);
		void LcdString(char *characters);
		void LcdWrite(byte dc, byte data);
		    

};
#endif